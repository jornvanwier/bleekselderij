use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::time::Duration;

use clap::{ArgEnum, Parser};
use tracing::info;
use tracing_subscriber;

use bleekselderij::registry::TaskRegistry;
use bleekselderij::task::{exec_remote, Task};
use bleekselderij::{storage, worker};

#[derive(ArgEnum, Clone)]
enum Mode {
    Client,
    Worker,
}

#[derive(Parser)]
#[clap(author, version, about)]
struct Cli {
    #[clap(arg_enum)]
    mode: Mode,
}

struct Example;

impl Task<'_> for Example {
    type Input = (String, usize);
    type Output = usize;

    fn call((value, extra): Self::Input) -> Self::Output {
        std::thread::sleep(Duration::from_secs(1));
        value.len() + extra
    }
}

#[tokio::main]
async fn main() {
    let args = Cli::parse();

    let redis = storage::connect().expect("connect storage");
    let mut tasks = TaskRegistry::new();
    tasks.register_task::<Example>();

    let mut hasher = DefaultHasher::new();
    tasks.hash(&mut hasher);
    let tasks_hash = hasher.finish();
    info!("Registry hash {tasks_hash:x}");

    match args.mode {
        Mode::Client => {
            tracing_subscriber::fmt::init();
            match exec_remote::<Example>(
                &redis,
                ("hello".to_string(), 1),
            )
            .await
            {
                Ok(output) => {
                    println!("Task retruned {:?}", output)
                }
                Err(e) => {
                    println!("Errored: {:?}", e)
                }
            }
        }
        Mode::Worker => worker::start_worker(&redis, tasks).await,
    }
}
