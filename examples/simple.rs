use clap::{ArgEnum, Parser};
use tracing::error;
use uuid::Uuid;

use bleekselderij::registry::TaskRegistry;
use bleekselderij::task::{delay_async, Task};
use bleekselderij::{storage, worker};

#[derive(ArgEnum, Clone)]
enum Mode {
    Client,
    Worker,
}

#[derive(Parser)]
#[clap(author, version, about)]
struct Cli {
    #[clap(arg_enum)]
    mode: Mode,
}

struct Example;

#[derive(serde::Deserialize, serde::Serialize, serde::Deserialize, serde::Serialize)]
struct ExampleArgs {
    test: String,
}

impl Task<'_> for Example {
    type Input = ExampleArgs;
    type Output = ();
    fn call(value: Self::Input) -> Self::Output {
        println!("I'm a task! This is my value: {}", value.test)
    }
}

#[tokio::main]
async fn main() {
    let args = Cli::parse();

    let redis = storage::connect().expect("connect storage");
    let mut tasks = TaskRegistry::new();
    tasks.register_task::<Example>();

    match args.mode {
        Mode::Client => {
            delay_async::<Example>(
                &redis,
                ExampleArgs {
                    test: "Hello!".into(),
                },
            )
            .await
            .expect("schedule task");
        }
        Mode::Worker => worker::start_worker(&redis, tasks).await,
    }
}
