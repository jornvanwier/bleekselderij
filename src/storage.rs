use std::time::{Duration, SystemTime};

use chrono::{DateTime, Utc};
use redis::aio::Connection;
use redis::{cmd, AsyncCommands, Client, Commands, Direction, RedisError};
use serde::Serialize;
use thiserror::Error;
use tracing::{info, trace};
use uuid::Uuid;

use crate::task::{TaskResult, TaskSubmission};
use crate::worker::{Worker, WorkerId};

const MASTER_TTL: usize = 2;
const MASTER_KEY: &str = "master";

#[derive(Error, Debug)]
pub enum StorageError {
    #[error("issue communicating with Redis")]
    Redis(#[from] RedisError),

    #[error("data could not be (de)serialized")]
    Serialization(#[from] serde_json::Error),
}

pub(crate) type Result<T> = core::result::Result<T, StorageError>;

pub fn connect() -> Result<Client> {
    let url = "redis://localhost:6379";
    let client = Client::open(url)?;
    let mut connection = client.get_connection().expect("connect");
    cmd("PING").execute(&mut connection);
    Ok(client)
}

pub(crate) async fn submit_task<A, S>(client: &Client, arguments: A, identifier: S) -> Result<Uuid>
where
    A: Serialize,
    S: Into<String>,
{
    let arguments = serde_json::to_string(&arguments)?;

    let entry = TaskSubmission {
        identifier: identifier.into(),
        job_id: Uuid::new_v4(),
        data: arguments,
    };
    let data = serde_json::to_string(&entry)?;

    let mut conn = client.get_async_connection().await?;
    let _: () = conn.lpush("work", data).await?;

    Ok(entry.job_id)
}

pub(crate) async fn get_task(client: &Client, worker: &Worker) -> Result<Option<TaskSubmission>> {
    let mut connection = client.get_async_connection().await?;
    let key = &format!("worker:{}:jobs", worker.id());

    let response: Option<(String, bool)> = redis::pipe()
        .blmove("work", key, Direction::Right, Direction::Left, 0)
        .expire(key, 60)
        .query_async(&mut connection)
        .await?;

    Ok(match response {
        Some((task, _)) => {
            let entry: TaskSubmission = serde_json::from_str(&task)?;
            Some(entry)
        }
        None => None,
    })
}

fn output_key(job_id: Uuid) -> String {
    format!("job:{}:output", job_id)
}

pub(crate) fn store_output<S>(
    client: &Client,
    worker: &Worker,
    job_id: Uuid,
    elapsed: Duration,
    output: S,
) -> Result<()>
where
    S: Serialize,
{
    let result = TaskResult {
        worker: worker.clone(),
        data: output,
        elapsed,
    };

    let payload = serde_json::to_string(&result)?;

    let mut connection = client.get_connection()?;
    // TODO do we have to use a list or stream?
    connection.lpush(output_key(job_id), payload)?;
    Ok(())
}

pub async fn get_output<'d, D>(client: &Client, job_id: Uuid) -> Result<Option<TaskResult<D>>>
where
    D: serde::de::DeserializeOwned,
{
    let mut connection = client.get_async_connection().await?;
    let result: Option<Vec<String>> = connection.brpop(output_key(job_id), 1000).await?;

    Ok(match result {
        Some(output) => Some(serde_json::from_str(&output[1])?),
        None => None,
    })
}

pub async fn check_master(client: &Client, worker: &Worker) -> Result<()> {
    let mut connection = client.get_async_connection().await?;

    let worker_id = worker.id();

    let result: Option<WorkerId> = redis::cmd("SET")
        .arg(MASTER_KEY)
        .arg(worker_id)
        .arg("GET")
        .arg("NX")
        .arg("EX")
        .arg(MASTER_TTL)
        .query_async(&mut connection)
        .await?;

    match result {
        Some(master) if &master == worker_id => {
            trace!("I am master, refreshing expiry");
            refresh_master(&mut connection).await?;
        }
        Some(master) => {
            trace!("{master} is master");
        }
        None => {
            info!("Won election");
            refresh_master(&mut connection).await?;
        }
    }

    Ok(())
}

async fn refresh_master(connection: &mut Connection) -> Result<()> {
    redis::pipe()
        .expire(MASTER_KEY, MASTER_TTL)
        .set(
            "master:heartbeat",
            <SystemTime as Into<DateTime<Utc>>>::into(SystemTime::now()).to_rfc3339(),
        )
        .query_async(connection)
        .await?;
    Ok(())
}
