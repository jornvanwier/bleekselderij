use std::fmt::{Display, Formatter};
use std::time::{Duration};

use random_word::Lang;
use redis::{FromRedisValue, RedisResult, RedisWrite, ToRedisArgs, Value};
use serde::{Deserialize, Serialize};
use tokio::{task};
use tracing::{error, info, info_span, trace, Instrument};

use crate::registry::TaskRegistry;
use crate::storage;
use crate::task::TaskSubmission;

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct WorkerId(String);

impl Display for WorkerId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl ToRedisArgs for WorkerId {
    fn write_redis_args<W>(&self, out: &mut W)
    where
        W: ?Sized + RedisWrite,
    {
        out.write_arg_fmt(&self.0);
    }
}

impl FromRedisValue for WorkerId {
    fn from_redis_value(v: &Value) -> RedisResult<Self> {
        Ok(WorkerId(String::from_redis_value(v)?))
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Worker {
    id: WorkerId,
}

impl Worker {
    pub fn id(&self) -> &WorkerId {
        &self.id
    }
}

pub async fn wait_for_task(redis: &redis::Client, worker: &Worker) -> TaskSubmission {
    loop {
        match storage::get_task(redis, worker).await {
            Ok(Some(task)) => return task,
            Err(e) => error!("Couldn't get next task: {:?}", e),
            _ => trace!("No task found"),
        }
    }
}

fn gen_worker_name() -> String {
    const WORDS: usize = 3;

    (0..WORDS)
        .map(|_| random_word::gen(Lang::En))
        .collect::<Vec<_>>()
        .join("-")
}

pub async fn start_worker(redis: &redis::Client, tasks: TaskRegistry) -> ! {
    tracing_subscriber::fmt().init();

    let worker_id = gen_worker_name();
    let worker = Worker {
        id: WorkerId(worker_id),
    };

    task::spawn(
        perform_bookkeeping(redis.clone(), worker.clone())
            .instrument(info_span!("", bookkeeping = worker.id().0)),
    );

    perform_tasks(redis.clone(), tasks, worker.clone())
        .instrument(info_span!("", worker = worker.id().0))
        .await
}

async fn perform_bookkeeping(client: redis::Client, worker: Worker) -> ! {
    info!("Start bookkeeping");
    loop {
        storage::check_master(&client, &worker)
            .await
            .expect("Master check failed");
        tokio::time::sleep(Duration::from_secs(1)).await;
    }
}

async fn perform_tasks(client: redis::Client, tasks: TaskRegistry, worker: Worker) -> ! {
    info!("Start worker");
    loop {
        let task = wait_for_task(&client, &worker).await;
        tasks.execute(&client, &task.identifier, &worker, task.job_id, &task.data);
    }
}
