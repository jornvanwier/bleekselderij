use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use tracing::{info, info_span};
use uuid::Uuid;
use crate::task::Task;
use crate::worker::Worker;

pub struct TaskRegistry {
    tasks: HashMap<&'static str, Box<dyn Fn(&redis::Client, &Worker, Uuid, &str)>>,
}

impl Hash for TaskRegistry {
    fn hash<H: Hasher>(&self, state: &mut H) {
        for key in self.tasks.keys() {
            key.hash(state);
        }
    }
}

impl TaskRegistry {
    pub fn new() -> Self {
        Self {
            tasks: HashMap::new(),
        }
    }

    pub fn register_task<T: for<'r> Task<'r> + Send>(&mut self) {
        let identifier = T::identifier();
        info!("Registering task {identifier}");

        self.tasks
            .entry(identifier)
            .or_insert(Box::new(|r, w, j, v| T::exec(r, w, j, v)));
    }

    pub fn execute(&self, redis: &redis::Client, identifier: &str, worker: &Worker, job_id: Uuid, data: &str) {
        let span = info_span!("", task=identifier);
        let _enter = span.enter();
        self.tasks[identifier](redis, worker, job_id, data)

    }
}
