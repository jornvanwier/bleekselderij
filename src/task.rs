use std::time::{Duration, Instant};

use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use tracing::{error, info};
use uuid::Uuid;

use crate::storage;
use crate::worker::Worker;

#[derive(serde::Deserialize, serde::Serialize)]
pub struct TaskSubmission {
    pub identifier: String,
    pub job_id: Uuid,
    pub data: String,
}

#[derive(serde::Deserialize, serde::Serialize)]
pub struct TaskResult<T> {
    pub worker: Worker,
    pub data: T,
    pub elapsed: Duration,
}

pub trait Task<'d> {
    type Input: Deserialize<'d> + Serialize + Send;
    type Output: DeserializeOwned + Serialize;

    fn call(arguments: Self::Input) -> Self::Output;

    fn identifier() -> &'static str {
        std::any::type_name::<Self>()
    }

    fn exec(redis: &redis::Client, worker: &Worker, job_id: Uuid, value: &'d str) {
        info!("Executing task ({})", job_id);

        let start = Instant::now();
        let arguments = serde_json::from_str(value).unwrap();
        let output = Self::call(arguments);
        let elapsed = start.elapsed();

        info!("Finished task in {:?}", elapsed);

        // todo only store if output is not ()
        if let Err(e) = storage::store_output(redis, worker, job_id, elapsed, output) {
            error!("Failed to store output for {}: {}", job_id, e);
        }
    }
}

pub async fn delay_async<'d, T>(redis: &redis::Client, arguments: T::Input) -> storage::Result<Uuid>
where
    T: Task<'d> + ?Sized,
{
    let identifier = T::identifier();
    storage::submit_task(redis, arguments, identifier).await
}

pub async fn exec_remote<'d, T>(
    redis: &redis::Client,
    arguments: T::Input,
) -> storage::Result<<T as Task<'d>>::Output>
where
    T: Task<'d>,
{
    let job_id = delay_async::<T>(redis, arguments).await?;

    loop {
        if let Some(result) = storage::get_output(redis, job_id).await? {
            info!(
                "Task {job_id} completed by worker {} in {:?}",
                result.worker.id(),
                result.elapsed
            );
            return Ok(result.data);
        }
    }
}
